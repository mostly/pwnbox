# pwnbox

Tool suite running on a Raspberry Pi powered PS3 fight stick implementing programmable macros.

## Daemon Conceptual Flow

On startup the RPi automatically logs in and starts the *pwnbox* daemon which performs the following tasks:
- INPUTS: folder containing macro files, gimx configuration file and a pwnbox configuration file.
- Check the macro folder and ensure the validity of the configuration file.
- Monitor lock switch
- Monitor kill switch
- Monitor macro shifter
- Set relevant values to LED-A and LED-B
- LED-A is dedicated to the *lock switch* and will illuminate either a locked or unlocked colour which can be configured in the *pwnbox configuration file*.
- LED-B is dedicated to the currently active macro it will show the colour associated with the macro and is set up in the *pwnbox configuration file*.
- Pressing the *macro shifter* button will: Kill gimx. Start gimx with the next macro in the *pwnbox configuration file*. Update the current RGB colour. This will only be triggered **IF the lock switch is disengaged!**
- Triggering the kill switch will kill the daemon cleanly and turn off the RPi. This will only be triggered **IF the lock switch is disengaged!**

The daemon will be addressing RPi GPIO pins so will need to be run as root. This means that *gimx* will be started by root which means that the *gimx* config folders need to reside in **/root**.

### Macro Input Folder

#### Macro files
These must be macro files compatible with *gimx* 4.2 and should be generated using the *pwnbox* utilities.

#### Macro configuration

- Lock switch unlocked colour
- Lock switch locked colour
- Macro 0 file : Macro 0 LED-B colour
- Macro X file : Macro X LED-B colour
- Starting controller address

## Utilities

The macro making scripts are implemented in Python 3.8 and should be run on a PC and then installed onto the RPi.

### Macro maker
**INPUTS**
- Core-map : A map with the entry format *peripheral-signal,gimx-name,ps3-button,game-command*
- Macro name : A string that will be used to generate the macro name and gimx configuration.

**OUTPUT**
- Macro compatible with *gimx* 4.2
- Configuration file compatible with *gimx* 4.2 

## Hardware

- Raspberry Pi model 1 B+
- Kingbright L-154A4SUREQBFZGEC RGB leds
- Sanwa jlf-tp-8yt joystick
- Sanwa obsf-30 and obsf-24 buttons
- TODO: Bluetooth dongle
- TODO: WiFi dongle
- TODO: Power Supply
- TODO: Peripheral usb board

## Software

- Gimx 4.2 (https://github.com/matlo/GIMX/releases/tag/v4.2) **gimx_4.2-1_armhf.deb**

### Installing gimx
sudo dpkg -i gimx_4.2-1_armhf.deb
sudo apt-get install libmhash2 libwxbase2.8-0 libwxgtk2.8-0 xterm bluez
sudo rpi-update
sudo shutdown -r now
sudo raspi-config
dmesg | grep dwc_otg
uname -a
/opt/vc/bin/vcgencmd version
sudo apt-get install gdebi
sudo apt-get -f install
sudo service triggerhappy stop
sudo update-rc.d triggerhappy disable
hciconfig -a hci0
sudo hciconfig hci0 revision

## Development Notes

### RPi Automatic Login
The RPi will be without a keyboard and without a monitor and accessible only by ssh. For that reason, we want to set it up to automatically login.
 
After the reboot, at the command line, enter:
	sudo nano /etc/inittab

Scroll down to the line:
	1:2345:respawn:/sbin/getty 115200 tty1
 
Comment it out:
	#1:2345:respawn:/sbin/getty 115200 tty1
 
Don't worry if the 115200 reads as 38400 or anything like that, we're just looking for the 1:2345 line
 
Add the line, beneath it:
	1:2345:respawn:/bin/login -f pi tty1 </dev/tty1 >/dev/tty1 2>&1
 
Take care to get the spaces exactly right.
 
Reboot and the pi should lautomatically login:
	sudo reboot

### Remove Development Limits on RPi 1 B+
There is a hidden config option in _/boot/config.txt_
	safe_mode_gpio=4

By default the USB ports are limited to **600mA**.
With the new option, this is increased to **1200mA**.

Note: this was just for internal testing. In subsequent firmware upgrades this should have been renamed to: 
	max_usb_current=1

The idea is you only set this option if you know you have a good (e.g. 2A) power supply.

### Bash Recipes
#### Setting the active macro
The file **<HOMEDIR>/.gimx/macros/configs.txt** contains the macros run by *gimx* 4.2. To set a macro, stop the existing instance of *gimx*, update the macro file as desired, restart *gimx*.

#### Starting gimx
For PS3:
	gimx --type Sixaxis --config <CONFIG> --hci 0 --bdaddr <ADDR> --status >> <LOGFILE>

Not supported now but for future purposes, PS4:
	gimx --config <CONFIG> --port /dev/ttyUSB0 --status >> <LOGFILE>

Config file paths will be relative to the run directory.

#### Changing bdaddr
Plug in a PS controller whose address you will clone to use in the *pwnbox* to the RPi via USB. Get the target address:
	sixaddr

This will print out 2 lines. Substring[3] of line[0] is **ps3Addr**. Substring[4] of line[1] is **controllerAddr**. Update the dongle address:
	bdaddr -r -i hci0 <controllerAddr>

**ps3Addr** is the address that is used to start *gimx*.

#### Is gimx running
	ps aux | grep '[g]imx' | awk '{print $2}'
    
Expected response is a PID.

#### Kill gimx
	kill $(ps aux | grep '[g]imx' | awk '{print $2}')

	Consider using pkill

